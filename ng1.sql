-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 29, 2015 at 11:36 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ng1`
--

-- --------------------------------------------------------

--
-- Table structure for table `speakers`
--

CREATE TABLE IF NOT EXISTS `speakers` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `speakers`
--

INSERT INTO `speakers` (`id`, `nombre`) VALUES
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, ''),
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `spk_coef`
--

CREATE TABLE IF NOT EXISTS `spk_coef` (
  `spk_id` int(11) DEFAULT NULL,
  `coef` varchar(200) DEFAULT NULL,
  `mean` varchar(200) DEFAULT NULL,
  `stdesv` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spk_coef`
--

INSERT INTO `spk_coef` (`spk_id`, `coef`, `mean`, `stdesv`) VALUES
(1, '0', '0.0', '0.0'),
(1, '1', '1.4220144239626509', '0.27419242660074544'),
(1, '2', '-0.5732865414299013', '0.5551034471168143'),
(1, '3', '0.07908009429803059', '0.29720351137359813'),
(1, '4', '-0.10903720460792467', '0.24721972486792762'),
(1, '5', '0.6217373324596507', '0.2143858382999657'),
(1, '6', '-0.7177262959065355', '0.1712364936025267'),
(1, '7', '0.2853760582190941', '0.2126036949158227'),
(1, '8', '-0.2099362714588316', '0.09311205598403859'),
(1, '9', '0.11274935023893855', '0.026260705535542748');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
